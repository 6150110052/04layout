package com.example.basicintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class SecondActivity4 extends AppCompatActivity {

    ImageButton sentbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second4);
    }

    public void Spotify (View view){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.spotify.com"));
        startActivity(intent);
    }

    public void Back(View view){
        sentbtn  = (ImageButton)findViewById(R.id.btnback);
        Intent intent = new Intent(SecondActivity4.this,SecondActivity2.class);
        startActivity(intent);
    }

}