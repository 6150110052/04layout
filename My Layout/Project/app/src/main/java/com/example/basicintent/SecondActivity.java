package com.example.basicintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent i = getIntent();
        String num1 = i.getStringExtra("num1");
        String num2 = i.getStringExtra("num2");
        TextView value1 = findViewById(R.id.txtVal1);
        TextView value2 = findViewById(R.id.txtVal2);
        value1.setText(num1);
        value2.setText(num2);

    }

    public void onClickNext(View view){
        Button Web = (Button)findViewById(R.id.btn3);
        Intent intent = new Intent(SecondActivity.this,SecondActivity2.class);
        startActivity(intent);
    }

    public void onClickNext1(View view){
        Button Logout = (Button)findViewById(R.id.btnBack);
        Intent intent = new Intent(SecondActivity.this,MainActivity.class);
        startActivity(intent);
    }




}